import ballerina/io;
import ballerina/graphql;

public type InputCovid record {|
    readonly string isoCode;
    string region;
    int active = 0;
    int cases = 0;
    int recovered = 0;
    int deaths = 0;
   
|};

table<InputCovid> key(isoCode) covidEntriesTable = table [
        {isoCode: "KH", region: "Khomas", cases: 0, deaths: 0, recovered: 0, active: 0},
        {isoCode: "OS", region: "Oshana", cases: 0, deaths: 0, recovered: 0, active: 0},
        {isoCode: "OM", region: "Omusati", cases: 0, deaths: 0, recovered: 0, active: 0},
        {isoCode: "HA", region: "Hardap", cases: 0, deaths: 0, recovered: 0, active: 0},
        {isoCode: "OE", region: "Okavango East", cases: 0, deaths: 0, recovered: 0, active: 0},
        {isoCode: "OW", region: "Okavango West", cases: 0, deaths: 0, recovered: 0, active: 0},
        {isoCode: "ER", region: "Erongo", cases: 0, deaths: 0, recovered: 0, active: 0},
        {isoCode: "OT", region: "Otjozondjupa", cases: 0, deaths: 0, recovered: 0, active: 0},
        {isoCode: "ZA", region: "Zambezi", cases: 0, deaths: 0, recovered: 0, active: 0}
    ];

public distinct service class CovidData {
    private final readonly & InputCovid entryRecord;

    function init(InputCovid entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
    }

    resource function get isoCode() returns string {
        return self.entryRecord.isoCode;
    }

    resource function get region() returns string {
        return self.entryRecord.region;
    }

    resource function get cases() returns int? {
        return self.entryRecord.cases;
    }

    resource function get deaths() returns int? {
        return self.entryRecord.deaths;
    }

    resource function get recovered() returns int? {
        return self.entryRecord.recovered;
    }

    resource function get active() returns int? {
        return self.entryRecord.active;
    }
}

public type Entry record {|
    int deaths = 0;
    int recovered = 0;
    int new_case = 0;
|};

service /graphql on new graphql:Listener(4000) {
    resource function get all() returns CovidData[] {
        InputCovid[] covidEntries = covidEntriesTable.toArray().cloneReadOnly();
        return covidEntries.map(entry => new CovidData(entry));
    }

    resource function get filter(string isoCode) returns CovidData? {
        InputCovid? InputCovid = covidEntriesTable[isoCode];
        if InputCovid is InputCovid {
            return new (InputCovid);
        }
        return;
    }

    remote function update(string isoCode, Entry entry) returns CovidData {
        InputCovid e = covidEntriesTable.get(isoCode);
        io:println(e.toBalString());

        if (entry.new_case > 0) {
            e.cases += entry.new_case;
            e.active += entry.new_case;
        }

        if (entry.deaths > 0) {
            e.deaths += entry.deaths;
            e.active -= entry.deaths;
        }

        if (entry.recovered > 0) {
            e.recovered += entry.recovered;
            e.active -= entry.recovered;
        }

        // covidEntriesTable.put(entry);
        return new CovidData(e);
    }
}
