import ballerina/http;
import ballerina/io;

public function main() returns error? {
    final http:Client clientBE = check new ("http://localhost:4000");

    check LoginUI(clientBE);
}

function LoginUI(http:Client clientBE) returns error? {
    io:println("SELECT YOUR DESIRED OPERATION");
    io:println("1. Retrieve Statistics");
    io:println("2. Retrieve by ISO");
    io:println("3. UPDATE STATISTICS");
    string choice = io:readln("Enter Number 1-3: ");

    json resp;

    if (choice == "1") {
        resp = check clientBE->post("/graphql", {query: "{ all { region cases active } }"});
        io:println(resp.toJsonString());
    } else if (choice == "2") {
        string iso = io:readln("Enter ISO: ");
        resp = check clientBE->post("/graphql", {query: "{ filter(isoCode: \"" + iso + "\" ) { region cases } }"});
        io:println(resp.toJsonString());

    } else if (choice == "3") {
        string iso = io:readln("Enter ISO: ");
        string cases = io:readln("Enter new cases: ");
        string deaths = io:readln("Enter recent deaths: ");
        string recovery = io:readln("Enter recoveries: ");

        resp = check clientBE->post("/graphql", {query: "mutation{ update(isoCode: \""+iso+"\", entry: {deaths: "+deaths+", recovered: "+recovery+", new_case: "+cases+"}) { region cases recovered deaths active } }"});
        io:println(resp.toJsonString());
    }


    check LoginUI(clientBE);

}
