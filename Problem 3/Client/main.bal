import ballerina/http;
import ballerina/io;

public function main() returns error? {
    final http:Client clientBE = check new ("http://localhost:4000");

    while (true) {

        io:println("Select an operation");
        io:println("1. Create student");
        io:println("2. Update student details");
        io:println("3. Update student course details");
        io:println("4. Find student");
        io:println("5. Retrieve all students");
        io:println("6. Remove student");
        string choice = io:readln("Enter number 1-6: ");
if (choice == "1") {
            string id = io:readln("Enter student number: ");
            string name = io:readln("Enter name: ");
            string email = io:readln("Enter email: ");
            string address = io:readln("Enter address: ");

            json resp = check clientBE->post("/create", {studentNumber: id, name: name, email: email, address: address});
            io:println(resp.toJsonString());

        } else if (choice == "2") {
            string studentNumber = io:readln("Enter student number: ");

            io:println("Update Value/leave empty to proceed");
            string name = io:readln("Enter name: ");
            string email = io:readln("Enter email: ");
            string address = io:readln("Enter address: ");

            json resp = check clientBE->put("/update", {studentNumber: studentNumber, name: name, email: email, address: address});
            io:println(resp.toJsonString());

        } else if (choice == "3") {
            string studentNumber = io:readln("Enter student number: ");
            string course = io:readln("Enter course code: ");

            json resp = check clientBE->put("/course", {studentNumber: studentNumber, course: course});
            io:println(resp.toJsonString());

        } else if (choice == "4") {
            string studentNumber = io:readln("Enter student number: ");

            json resp = check clientBE->get("/lookup?studentNumber=" + studentNumber);
            io:println(resp);
        } else if (choice == "5") {
            json resp = check clientBE->get("/all");
            io:println(resp);
        } else if (choice == "6") {
            string studentNumber = io:readln("Enter student number: ");
            json resp = check clientBE->delete("/delete?studentNumber=" + studentNumber);
            io:println(resp);
        }
    }
}
